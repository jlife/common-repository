package com.jlife.common.audit;

/**
* @author Dzmitry Misiuk
*/
public enum AccessType {
    PUBLIC,
    PRIVATE,
    HIDDEN
}
