package com.jlife.common.audit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.stereotype.Component;

@Component()
@Scope(value = "singleton")
public class BeforeDeleteListener implements
        ApplicationListener<BeforeDeleteEvent<Object>> {

    @Autowired
    private AuditorAware<String> auditorAware;


    @Autowired
    private AuditingSaver auditingSaver;

    public BeforeDeleteListener() {
    }

    @Override
    public void onApplicationEvent(BeforeDeleteEvent<Object> event) {
        String id = (String) event.getDBObject().get("id");
        Class clazz = event.getType();
        if (AuditingSaver.isAuditingEntity(clazz)) {
            auditingSaver.setBefore(id, clazz);
        }
    }
}