package com.jlife.common.audit;


import com.jlife.common.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;

@Component()
@Scope(value = "singleton")
public class BeforeSaveListener implements
        ApplicationListener<BeforeSaveEvent<Object>> {

    @Autowired
    private AuditorAware<String> auditorAware;

    @Autowired
    private AuditingSaver auditingSaver;

    public BeforeSaveListener() {
    }

    @Override
    public void onApplicationEvent(BeforeSaveEvent<Object> event) {
        Object obj = event.getSource();
        Class clazz = obj.getClass();
        if (AuditingSaver.isAuditingEntity(clazz)) {
            Entity entity = (Entity) obj;
            auditingSaver.setBefore(entity);
        }
    }
}