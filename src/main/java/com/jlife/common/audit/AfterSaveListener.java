package com.jlife.common.audit;


import com.jlife.common.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.stereotype.Component;

@Component()
@Scope(value = "singleton")
public class AfterSaveListener implements
        ApplicationListener<AfterSaveEvent<Object>> {

    @Autowired
    private AuditorAware<String> auditorAware;

    @Autowired
    private AuditingSaver auditingSaver;

    public AfterSaveListener() {
    }

    @Override
    public void onApplicationEvent(AfterSaveEvent<Object> event) {
        Object obj = event.getSource();
        Class clazz = obj.getClass();
        if (AuditingSaver.isAuditingEntity(clazz)) {
            Entity entity = (Entity) obj;
            auditingSaver.setAfter(entity);
            auditingSaver.save();
        }
    }

}