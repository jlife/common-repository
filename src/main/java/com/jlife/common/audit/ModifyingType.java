package com.jlife.common.audit;

/**
 * @author Dzmitry Misiuk
 */
public enum  ModifyingType {
    UPDATE, DELETE, CREATE
}
