package com.jlife.common.audit;

import com.jlife.common.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

/**
 * @author Dzmitry Misiuk
 */
@Component
@Scope(value = "singleton")
public class AuditingSaver {

    @Autowired
    private MongoTemplate mongoTemplate;

    private final ThreadLocal<Entity> before = new ThreadLocal<>();

    private final ThreadLocal<Entity> after = new ThreadLocal<>();

    public void setBefore(String id, Class clazz){
        Query query = new Query(new Criteria("_id").is(id));
        Entity before = (Entity) mongoTemplate.findOne(query, clazz);
        this.before.set(before);
    }

    public void setBefore(Entity entity) {
        this.before.set(null);
        this.after.set(null);
        if (entity == null) {
            throw new IllegalArgumentException("Entity before save should be not null");
        }
        String id = entity.getId();
        if (id != null) {
            setBefore(id, entity.getClass());
        }
        // todo check insert new with id and version
    }

    protected void setAfter(Entity after) {
        // todo check for deleting
        this.after.set(after);
    }

    protected AuditDocument getAudit() {
        Entity before = this.before.get();
        Entity after = this.after.get();
        AuditDocument auditDocument = new AuditDocument(before, after);

        return auditDocument;
    }

    protected AuditDocument save() {
        AuditDocument auditDocument = getAudit();
        mongoTemplate.save(auditDocument);
        this.before.set(null);
        this.after.set(null);
        return auditDocument;
    }

    public static boolean isAuditingEntity(Class clazz){
        boolean isAuditAnnotated = clazz.isAnnotationPresent(Audit.class);
        boolean isAuditDocument = AuditDocument.class.equals(clazz);
        boolean isEntity = Entity.class.isAssignableFrom(clazz);
        return isEntity && isAuditAnnotated && !isAuditDocument;
    }
}
