package com.jlife.common.audit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.stereotype.Component;

@Component()
@Scope(value = "singleton")
public class AfterDeleteListener implements
        ApplicationListener<AfterDeleteEvent<Object>> {

    @Autowired
    private AuditorAware<String> auditorAware;

    @Autowired
    private AuditingSaver auditingSaver;

    public AfterDeleteListener() {
    }

    @Override
    public void onApplicationEvent(AfterDeleteEvent<Object> event) {
        Class clazz = event.getType();
        if (AuditingSaver.isAuditingEntity(clazz)) {
            auditingSaver.setAfter(null);
            auditingSaver.save();
        }
    }

}