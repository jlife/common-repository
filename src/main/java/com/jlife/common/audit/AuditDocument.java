package com.jlife.common.audit;

import com.jlife.common.entity.Entity;
import org.springframework.data.domain.Auditable;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Class to store information about changing of object.
 *
 * @author Dzmitry Misiuk
 */
@Document(collection = "audit")
public class AuditDocument extends Entity implements Auditable<String, String> {


    private String entityClass;

    private String entityId;

    private ModifyingType modifyingType;

    private AccessType accessType = AccessType.PRIVATE;

    private Entity before;

    private Entity after;

    private List<String> users;

    public AuditDocument() {
    }

    public AuditDocument(Entity before, Entity after) {
        this.before = before;
        this.after = after;
        boolean isNewEntity = before == null;
        this.entityClass = (!isNewEntity) ? before.getClass().getCanonicalName() :
                after.getClass().getCanonicalName();
        if (!isNewEntity) {
            this.entityId = before.getId();
            this.modifyingType = (after == null) ? ModifyingType.DELETE : ModifyingType.UPDATE;
        }
        if (isNewEntity) {
            this.modifyingType = ModifyingType.CREATE;
            this.entityId = after.getId();
        }
    }

    public Entity getBefore() {
        return before;
    }

    public void setBefore(Entity before) {
        this.before = before;
    }

    public Entity getAfter() {
        return after;
    }

    public void setAfter(Entity after) {
        this.after = after;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public AccessType getAccessType() {
        return accessType;
    }

    public void setAccessType(AccessType accessType) {
        this.accessType = accessType;
    }

    public String getEntityClass() {
        return entityClass;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public ModifyingType getModifyingType() {
        return modifyingType;
    }

    @Override
    public boolean isNew() {
        return this.getId() == null;
    }

}