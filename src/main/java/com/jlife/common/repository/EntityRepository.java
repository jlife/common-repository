package com.jlife.common.repository;

import com.jlife.common.entity.Entity;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * public interface to work with Entity.
 *
 * @author Dzmitry Misiuk
 */
@NoRepositoryBean
public interface EntityRepository<T extends Entity> extends PagingAndSortingRepository<T, String> {

    /**
     * Returns all instances of the type those were created by the given creator.
     *
     * @param createdBy creator id
     * @return all relevant entities
     */
    List<T> findByCreatedBy(String createdBy);
}