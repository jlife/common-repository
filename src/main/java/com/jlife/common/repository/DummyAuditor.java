package com.jlife.common.repository;

import org.springframework.data.domain.AuditorAware;

/**
 * @author Dzmitry Misiuk
 */
public class DummyAuditor implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
        return "dummy-modifier";
    }
}
