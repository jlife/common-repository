package com.jlife.common.repository;


import com.jlife.common.entity.SimpleEntity;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Dzmitry Misiuk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/repository-test.xml"})
public class SimpleEntityRepositoryTest {

    @Autowired
    private SimpleEntityRepository simpleEntityRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    @After
    public void cleanDb() {
        mongoTemplate.dropCollection(SimpleEntity.class);
    }

    @Test
    public void testFindSimpleEntities() {
        Page<SimpleEntity> persons = simpleEntityRepository.findAll(new PageRequest(0, 10));
    }

    @Test
    public void testCreateAndReadEntity() {
        SimpleEntity se = new SimpleEntity();
        String id = new ObjectId().toString();
        se.setId(id);
        simpleEntityRepository.save(se);
        SimpleEntity readEntity = simpleEntityRepository.findOne(id);
        assertNotNull(readEntity);
        assertNotNull(readEntity.getLastModifiedBy());
        assertNotNull(readEntity.getCreatedBy());
        assertNotNull(readEntity.getLastModifiedDate());
        assertEquals(new Long(0), readEntity.getVersion());
        SimpleEntity updatedEntity = simpleEntityRepository.save(readEntity);
        assertEquals(new Long(1), updatedEntity.getVersion());
    }
}