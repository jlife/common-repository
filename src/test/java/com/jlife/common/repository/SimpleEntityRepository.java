package com.jlife.common.repository;

import com.jlife.common.entity.SimpleEntity;

/**
 * Repository to work with Simple Entity.
 *
 * @author Dzmitry Misiuk
 */
public interface SimpleEntityRepository extends EntityRepository<SimpleEntity> {
}