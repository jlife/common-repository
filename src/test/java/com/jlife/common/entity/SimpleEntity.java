package com.jlife.common.entity;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Class to work with user.
 *
 * @author Dzmitry Misiuk
 * @author Khralovich Dzmitry
 */
@Document(collection = "entities")
public class SimpleEntity extends Entity {
}
